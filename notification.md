[![alt text](http://www.unifyed.com/wp-content/uploads/2017/08/favicon.ico "Powered by Unifyed")](https://unifyed.com)

# Message API

> Messaging Service is to send either or both **PUSH Notifications & SMS** to platform users
  - Direct SMS
  - SNS based Messaging
  - GCM based Android Notifications
  - APN based iOS Notifications

### Tech Stack

> This service uses below listed technologies:

* [RabbitMQ] - for queuing messages received at endpoint!
* [node.js] - evented I/O for the backend
* [Express] - fast node.js network app framework
* [MongoDB] - stores messages for users as there Notification Centre
* [AWS SNS] - sends messages on mobile numbers
* [GCM] - sends notifications on Android app users
* [APNs] - sends notifications on iOS app users
* [CronJob] - for scheduled messages

### API(s)

#### Authentication
> fetch authorization token for headers in API's

| Method | Endpoint  | Header | Body | Response |
| ------ | ------ | ------ | ------ | ------ |
| `POST` | /unifyd-gateway/api/unifydidentity/open/oauth2/token | accept: application/json, content-type: application/x-www-form-urlencoded, x-tenant-domain: **_registeredDomain_**, x-tenant-id: **_tenantId_** | username=username@domain.edu&password=AlphaNum$plC#r | ```{access_token: "VARCHAR(36)",refresh_token: "VARCHAR(36)",scope: "openid",id_token: "TEXT",token_type: "Bearer",expires_in: SEC}``` |

#### Message API(s)
> prepend `/unifyd-gateway/api/unifyed-push/` in URI

##### Mandatory headers
```
authorization: Bearer e47f1a40-ab24-3ed0-8eb6-fba8d75f65b0
content-type: application/json
principal-user: {admin}
x-tenant-domain: {tenant_domain}
x-tenant-id: {tenant_id}
```

| Method | Endpoint  | Body | Response |
| ------ | ------ | ------ | ------ |
| `POST` | message |  ```{channels:["PUSH"],message:"Lorem Ipsum is simply dummy text of type specimen book.",title:"Test Title",groups:[],users:["username@domain.com"]}```| ```{status:201,http:"CREATED",info:"Resource was successfully created.",data:{channels:["PUSH"],message:"Lorem Ipsum is simply dummy text of type specimen book.",title:"Test Title",groups:[],users:["username@domain.com"],createdBy:"{admin}",schedule:"YYYY-MM-DDTHH:MM:SS.MILLZ",createdAt:"YYYY-MM-DDTHH:MM:SS.MILLZ",_id:"ALPHANUMERICZ09876543210"}} ``` |
| `HEAD` | messages | | _Response.Headers_ ``` {"x-count": NUM} ```|
| `GET` | messages/**_:limit_**/**_:skip_** | | ``` {status:200,http:"OK",info:"Request was successful.",data:[{_id:"ALPHANUMERICZ09876543210",channels:["PUSH"],message:"Lorem Ipsum is simply dummy text of type specimen book.",title:"Test Title",groups:[],users:["username@domain.com"],createdBy:"{admin}",schedule:"YYYY-MM-DDTHH:MM:SS.MILLZ",createdAt:"YYYY-MM-DDTHH:MM:SS.MILLZ"},...]} ``` |
| `GET` | message/**:_id_** | |``` {status:200,http:"OK",info:"Request was successful.",data:{_id:"ALPHANUMERICZ09876543210",channels:["PUSH"],message:"Lorem Ipsum is simply dummy text of type specimen book.",title:"Test Title",groups:[],users:["username@domain.com"],createdBy:"{admin}",schedule:"YYYY-MM-DDTHH:MM:SS.MILLZ",createdAt:"YYYY-MM-DDTHH:MM:SS.MILLZ"}} ```|
| `PUT` | message/**:_id_** |```{channels:["PUSH"],message:"Lorem Ipsum is simply dummy text of type specimen books.",title:"Test Title Updated",groups:[],users:["username2@domain.com"],schedule:"YYYY-MM-DDTHH:MM:SS.MILLZ"}```|``` {status:200,http:"OK",info:"Request was successful."} ```|
| `DELETE` | message/**:_id_** ||``` {status:200,http:"OK",info:"Request was successful."} ```|

### Todos:
***

 - Multi threading
 - Category Management
 - Override


   [node.js]: <http://nodejs.org>
   [express]: <http://expressjs.com>
   [RabbitMQ]: <https://www.rabbitmq.com/>
   [MongoDB]: <https://www.mongodb.com/>
   [AWS SNS]: <https://aws.amazon.com/SNS>
   [GCM]: <https://developers.google.com/cloud-messaging/>
   [APNs]: <https://developer.apple.com/go/?id=push-notifications>
   [CronJob]: <https://en.wikipedia.org/wiki/Cron>